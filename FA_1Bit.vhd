----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:18:44 07/16/2018 
-- Design Name: 
-- Module Name:    FA_1Bit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FA_1Bit is
	port(
			A		:in	std_logic;
			B		:in	std_logic;
			Cin	:in 	std_logic;
			S		:out	std_logic;
			Cout	:out	std_logic
			);

end FA_1Bit;

architecture Behavioral of FA_1Bit is

begin
	S		<=	A xor B xor Cin;
	cout	<=(A and B)or(A and Cin)or(B and Cin);

end Behavioral;

