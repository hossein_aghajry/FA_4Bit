----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:21:58 07/16/2018 
-- Design Name: 
-- Module Name:    FA_4Bit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FA_4Bit is
	port(
				A		:in 	std_logic_vector(3 downto 0);
				B		:in	std_logic_vector(3 downto 0);
				Cin	:in	std_logic;
				S		:out 	std_logic_vector(3 downto 0);
				Cout	:out	std_logic
			);
end FA_4Bit;

architecture Behavioral of FA_4Bit is

		COMPONENT FA_1Bit
	PORT(
		A : IN std_logic;
		B : IN std_logic;
		Cin : IN std_logic;          
		S : OUT std_logic;
		Cout : OUT std_logic
		);
	END COMPONENT;

	signal C	:	std_logic_vector(2 downto 0)	:="000";

begin
	FA0: FA_1Bit PORT MAP(
		A 		=> A(0),
		B 		=> B(0),
		Cin 	=> Cin,
		S 		=> S(0),
		Cout 	=>	C(0) 
	);
	
	FA1: FA_1Bit PORT MAP(
		A 		=> A(1),
		B 		=> B(1),
		Cin 	=> C(0),
		S 		=> S(1),
		Cout 	=>	C(1) 
	);
	
	FA2: FA_1Bit PORT MAP(
		A 		=> A(2),
		B 		=> B(2),
		Cin 	=> C(1),
		S 		=> S(2),
		Cout 	=>	C(2) 
	);
	
	FA3: FA_1Bit PORT MAP(
		A 		=> A(3),
		B 		=> B(3),
		Cin 	=> C(2),
		S 		=> S(3),
		Cout 	=>	Cout 
	);
	

end Behavioral;

