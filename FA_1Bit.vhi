
-- VHDL Instantiation Created from source file FA_1Bit.vhd -- 11:24:22 07/16/2018
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT FA_1Bit
	PORT(
		A : IN std_logic;
		B : IN std_logic;
		Cin : IN std_logic;          
		S : OUT std_logic;
		Cout : OUT std_logic
		);
	END COMPONENT;

	Inst_FA_1Bit: FA_1Bit PORT MAP(
		A => ,
		B => ,
		Cin => ,
		S => ,
		Cout => 
	);


